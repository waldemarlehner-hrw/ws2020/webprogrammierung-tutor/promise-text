module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
      ? '/ws2020/webprogrammierung-tutor/promise-text/'
      : '/'
  }